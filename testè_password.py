#import requests
#Demande à l'utilisateur de rentrer le mot de passe à vérifier
user_password = input("Veuillez entrer le mot de passe à vérifier: \n")

#Créer une fonction pour récupérer les 500 mots de passe populaires et comparer
def worst_500_password(password):
    #Download la SecList
    
    print("Téléchargement de la liste des 500 mots de passe...")
    url = 'https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/500-worst-passwords.txt'
    retrieve = requests.get(url)
    #Stocker le contenue dans la variable content
    content = retrieve.content

    if str(password) in str(content):
        print("Mot de passe correspond dans les 500 pires mot de passe...")
        exit(0)
    
    if __name__ == "__main__":
        worst_500_password(user_password)
